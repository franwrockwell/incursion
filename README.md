So, you'd like to install Incursion?

Good. We're gonna need all the help we can get.

Now, whether you consider yourself a scientist or a soldier, you're gonna need to follow instructions - carefully.

How to install our game:

1) Download the game files and unzip them.

Let's see if you can handle that...

2) Our game runs on a developer app id from Steam. If you don't have Steam or an account, you'll need to download it and make one (it's free). More on that here:
https://support.steampowered.com/kb_article.php?ref=3046-ECVN-3712

3) You'll need to log into Steam to connect to other players in our competitive sandbox. Do so now.

Once you have Steam opened up:
1. Click on the "Steam" button in the top left corner.
2. Click on the settings tab.
3. Go to the Downloads options.
4. Set your download region to Los Angeles.

Congrats. You're ready for the hard part.

4) This is the hardest step, so pay attention here. You'll need to find at least one other friend to play with you in this player vs player sandbox game.
So, go on Facebook, Discord, or your favorite communication channels and find 1-3 other friends to try Incursion with! They'll have to follow these same steps here.

5) At the same time as your friends, double click on Incursion.exe. This will launch the game.

6) Incursion uses Peer-to-Peer connections for the scope of the student project. The RTS player (The Transcendents) will host the game while the
FPS players (The Communion) will joining the host. Take your time here and choose wisely...

7) The Transcendents (RTS player) will always be the lobby owner. Decide who this will be and have them name the lobby.
The rest of the team will join using as The Communion (FPS players). Due to the developer app id at this time, you'll have to join within 10 seconds of the lobby creation.

8) Go ahead and ready up. The RTS player will start the game once everyone is ready. Have fun and welcome to Incursion.

Try not to get blown up...

-- Dr. Adrias